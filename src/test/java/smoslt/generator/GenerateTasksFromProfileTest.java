package smoslt.generator;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import smoslt.domain.ProjectProfile;

public class GenerateTasksFromProfileTest {
	File sourceFile = new File("META-INF/config/projectProfile_a.json");
	ProjectProfile projectProfile = ProjectProfileFactory.get(sourceFile);
	ScheduleFromProfile scheduleFromProfile = new ScheduleFromProfile(
			projectProfile);

	/*
	 * Started to write this test and then abandoned effort due to change in
	 * design and basic laziness
	 */
	@Test
	public void testWriteJsonFromProjectProfile() {
		GenerateTasksFromProfile generateTasksFromProfile = new GenerateTasksFromProfile(
				projectProfile, scheduleFromProfile.getScheduleClone());
	}

}
