package smoslt.generator;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;

import org.junit.Test;

import smoslt.domain.Models;
import smoslt.domain.ProjectProfile;
import smoslt.domain.Schedule;
import smoslt.domain.ScheduleBuild;
import smoslt.domain.Task;
import smoslt.mprtxprt.ImportFromProjectLibre;

public class ScheduleFromProfileTest {
	File productFile = new File(
			"./src/test/resources/3dd2dda8-b6c8-420b-955b-c0cef3d61add.pod");
	File sourceFile = new File(
			"META-INF/config/projectProfile_a.json");
	ProjectProfile projectProfile = ProjectProfileFactory.get(sourceFile);

	@Test
	public void testExecute() {
		deleteProductFile();
		assertProductFileDoesNotExist();
		assertSourceFileExists();
		addModels();
		ScheduleFromProfile scheduleFromProfile = new ScheduleFromProfile(projectProfile);
		/*
		 * only runs in IDE has classpath problems running in maven surefire
		 */
//		scheduleFromProfile.execute(productFile);
//		Schedule mySchedule = scheduleFromProfile.getScheduleClone();
//		assertTrue( mySchedule.getAssignments().size()>100);
////		for(Task task:mySchedule.getTaskList()){
////			System.out.println(task.getResourceSpecifications().get(0));
////			System.out.println(task.getSpecifiedResources().size());
////		}
//		assertTrue(productFile.exists());
//		ScheduleBuild scheduleBuild = new ImportFromProjectLibre(productFile);
//		Schedule schedule = new Schedule(scheduleBuild);
//		printTasks(schedule);
////		deleteProductFile();
	}


	private void addModels() {
		projectProfile.getProjectFactors().get(GeneratorHelper.PF_DOMAIN_TABLE_NAMES).setText(sampleModels());
	}

	private String sampleModels() {
		StringBuffer sb = new StringBuffer();
		String[] source = Models.LOGISTICS_MODELS.split("\n");
		for(int i=0;i<10;i++){
			sb.append(source[i] + "\n");
		}
		return sb.toString();
	}

	private void printTasks(Schedule schedule) {
		for(Task task:schedule.getTaskList()){
			System.out.println("YO "+task.getName() + " - " + resourcesAsString(task));
		}
	}

	private String resourcesAsString(Task task) {
		StringBuffer sb = new StringBuffer();
		for(String reqs:task.getResourceSpecifications()){
			sb.append(" " +reqs );
		}
		return sb.toString().trim();
	}

	private void deleteProductFile() {
		productFile.delete();
		assertProductFileDoesNotExist();
	}

	private void assertProductFileDoesNotExist() {
		assertFalse(productFile.exists());
	}

	private void assertSourceFileExists() {
		assertTrue(sourceFile.exists());
	}

}
