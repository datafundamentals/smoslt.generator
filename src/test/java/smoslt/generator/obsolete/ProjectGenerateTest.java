package smoslt.generator.obsolete;

import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import smoslt.domain.ProjectProfile;

public class ProjectGenerateTest {

	@Test
	public void testCreateComponents() throws JsonParseException,
			JsonMappingException, IOException {
		ProjectGenerate projectGenerate = new ProjectGenerate();
		projectGenerate.createComponents();
		projectGenerate.write();
		ObjectMapper objectMapper = new ObjectMapper();
		ProjectProfile projectProfile = objectMapper
				.readValue(
						new File("META-INF/config/projectProfile_a.json"),
						ProjectProfile.class);
		// ProjectProfile projectProfile = objectMapper.readValue(new
		// File(System.getProperty("WORKSPACE")+"smoslt.weben/META-INF/config/projectProfile_a.json"),
		// ProjectProfile.class);
		assertNotNull(projectProfile);
	}

}
