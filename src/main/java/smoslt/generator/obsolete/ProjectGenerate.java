package smoslt.generator.obsolete;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import smoslt.domain.ProjectFactor;
import smoslt.domain.ProjectProfile;

public class ProjectGenerate {
	// private static final String SOMETHING_STYLE = "";
	// private static final String SOMETHING_STYLE1 = "";
	// private static final String SOMETHING_STYLE2 = "";
	// private static final String SOMETHING_STYLE3 = "";
	// private static final String SOMETHING_STYLE4 = "";
	// private static final String[] SOMETHING_STYLE_OPTIONS = {
	// SOMETHING_STYLE1,
	// SOMETHING_STYLE2, SOMETHING_STYLE3, SOMETHING_STYLE4 };
	private static final String HOW_MANY_DEVELOPERS = "How Many Developers?";
	private static final String HOW_MANY_OPS_ENGINEERS = "How Many Ops Engineers?";
	private static final String HOW_MANY_QA_TESTERS = "How Many QA Testers?";
	private static final String HOW_MANY_MANAGERS = "How Many Managers/Directors?";
	private static final String HOW_MANY_BA = "How Many Business Analysts?";
	private static final String HOW_MANY_DBA = "How Many Database Admins?";
	private static final String HOW_MANY_UI_DESIGNERS = "How Many UI Designers";
	private static final String HIRING_STYLE = "Hiring Style";
	private static final String HIRING_STYLE1 = "We hire friends of team";
	private static final String HIRING_STYLE2 = "We interview a couple folks first";
	private static final String HIRING_STYLE3 = "We have a formal process, tests, interviews";
	private static final String HIRING_STYLE4 = "Ridiculously extensive process, very low turnover after hired";
	private static final String[] HIRING_STYLE_OPTIONS = { HIRING_STYLE1,
			HIRING_STYLE2, HIRING_STYLE3, HIRING_STYLE4 };
	private static final String AVERAGE_YEARS_EXPERIENCE = "Average Years of Experience";
	private static final String CULTURE_TYPE = "Type of Culture";
	private static final String CULTURE_TYPE1 = "Laid back style - Bring your dog, work 6 hours or so, go to the brew pub.";
	private static final String CULTURE_TYPE2 = "Normal corporate culture";
	private static final String CULTURE_TYPE3 = "Older guys nobody questions anything, but almost all solid hitters";
	private static final String CULTURE_TYPE4 = "Top dollar Ivy League graduates 60 hours week of test driven culture";
	private static final String[] CULTURE_TYPE_OPTIONS = { CULTURE_TYPE1,
			CULTURE_TYPE2, CULTURE_TYPE3, CULTURE_TYPE4 };
	private static final String DOCUMENTATION_STYLE = "Documentation Style";
	private static final String DOCUMENTATION_STYLE1 = "Yeah, we're gonna do that someday";
	private static final String DOCUMENTATION_STYLE2 = "Some code had some documentation written about it";
	private static final String DOCUMENTATION_STYLE3 = "We have a wiki, lots of people use it when they need to document something";
	private static final String DOCUMENTATION_STYLE4 = "No code goes to production without rigorous documenation against several checklists";
	private static final String[] DOCUMENTATION_STYLE_OPTIONS = {
			DOCUMENTATION_STYLE1, DOCUMENTATION_STYLE2, DOCUMENTATION_STYLE3,
			DOCUMENTATION_STYLE4 };
	private static final String TESTING_STYLE = "Testing Practices";
	private static final String TESTING_STYLE1 = "Our customers can be counted on to tell us if something is broken";
	private static final String TESTING_STYLE2 = "Our devs are pros, certainly they would test their own work?";
	private static final String TESTING_STYLE3 = "We have a testing department that does our testing for us!";
	private static final String TESTING_STYLE4 = "We run junit before we commit our code. Some code is tested.";
	private static final String TESTING_STYLE5 = "All code is regression tested before it is committed, and run w every build";
	private static final String[] TESTING_STYLE_OPTIONS = { TESTING_STYLE1,
			TESTING_STYLE2, TESTING_STYLE3, TESTING_STYLE4, TESTING_STYLE5 };
	private static final String CD_STYLE = "Continuous Delivery Practices";
	private static final String CD_STYLE1 = "One of our architects went to a Continuous Delivery presentation once";
	private static final String CD_STYLE2 = "We have a Hudson server that a developer set up a couple years ago";
	private static final String CD_STYLE3 = "We use our Jenkins server to run our unit tests";
	private static final String CD_STYLE4 = "All of our releases are built from our Bamboo server and deployed in production";
	private static final String[] CD_STYLE_OPTIONS = { CD_STYLE1, CD_STYLE2,
			CD_STYLE3, CD_STYLE4 };
	private static final String QUALITY_STYLE = "Quality Scale";
	private static final String QUALITY_STYLE1 = "Production? Prototype? Proof of Concept? What's the difference? ";
	private static final String QUALITY_STYLE2 = "Our projects mostly work well, eventually.";
	private static final String QUALITY_STYLE3 = "We have tough standards.";
	private static final String QUALITY_STYLE4 = "NASA launch software couldn't be more bulletproof.";
	private static final String[] QUALITY_STYLE_OPTIONS = { QUALITY_STYLE1,
			QUALITY_STYLE2, QUALITY_STYLE3, QUALITY_STYLE4 };
	private static final String WHICH_LANGUAGES_THIS_PROJECT = "Which Languages This Project?";
	private static final String BACK_FRONT_END = "Back End to Front End Percentage of Development Work - (100% Back End = 0   -   100% Front End = 100)";
	private static final String JAVA_MODULARITY_STYLE = "Modularity of Java CodeBase";
	private static final String JAVA_MODULARITY_STYLE1 = "Big Ball of Mud - Standard Java";
	private static final String JAVA_MODULARITY_STYLE2 = "Separate Code Modules/Projects Deployed as Independent Jars";
	private static final String JAVA_MODULARITY_STYLE3 = "Some experimentation with OSGi";
	private static final String JAVA_MODULARITY_STYLE4 = "Veteran OSGi Shop";
	private static final String[] JAVA_MODULARITY_STYLE_OPTIONS = {
			JAVA_MODULARITY_STYLE1, JAVA_MODULARITY_STYLE2,
			JAVA_MODULARITY_STYLE3, JAVA_MODULARITY_STYLE4 };
	private static final String CONTAINER_MODULARITY_STYLE = "Service Modularity Practices";
	private static final String CONTAINER_MODULARITY_STYLE1 = "So exactly what do you mean by 'service', anyway?";
	private static final String CONTAINER_MODULARITY_STYLE2 = "Separate services installed on various servers";
	private static final String CONTAINER_MODULARITY_STYLE3 = "We use a service bus";
	private static final String CONTAINER_MODULARITY_STYLE4 = "Every service is on it's own [Docker?] container(s)";
	private static final String[] CONTAINER_MODULARITY_STYLE_OPTIONS = {
			CONTAINER_MODULARITY_STYLE1, CONTAINER_MODULARITY_STYLE2,
			CONTAINER_MODULARITY_STYLE3, CONTAINER_MODULARITY_STYLE4 };
	private static final String DOMAIN_SAMPLE_SET = "Domain Sample Set";
	private static final String DOMAIN_SAMPLE_SET1 = "Retail Orders";
	private static final String DOMAIN_SAMPLE_SET2 = "Manufacturing Line";
	private static final String DOMAIN_SAMPLE_SET3 = "Web Traffic Monitor";
	private static final String DOMAIN_SAMPLE_SET4 = "Social Graph";
	/*
	 * Business API Mobile App Content Delivery Batch Transform Big Data ETL &
	 * Analysis Desktop Data Tool Corporate BI Rollout Streaming Data Input
	 * Service ETL and Analysis
	 */
	private static final String[] DOMAIN_SAMPLE_SET_OPTIONS = {
			DOMAIN_SAMPLE_SET1, DOMAIN_SAMPLE_SET2, DOMAIN_SAMPLE_SET3,
			DOMAIN_SAMPLE_SET4 };
	private static final String HOW_MANY_TABLES = "How Many Tables?";
	private static final String DEGREE_OF_DATA_CUSTOMIZATION = "Degree of Customization Required Between Persistence and Display of Tabular Data";
	private static final String DESIGN_STYLE = "Design First vs Just Do It";
	private static final String DESIGN_STYLE1 = "0% - Our programmers design every UI, every process - they are professionals, after all";
	private static final String DESIGN_STYLE2 = "25% - We have meetings sometimes, and design is discussed in a cursory way";
	private static final String DESIGN_STYLE3 = "50% - Rebecca oversees design, and meets with programmers";
	private static final String DESIGN_STYLE4 = "100% - No code is written without a design, first. We have a published design standards that are rigorously enforced";
	private static final String[] DESIGN_STYLE_OPTIONS = { DESIGN_STYLE1,
			DESIGN_STYLE2, DESIGN_STYLE3, DESIGN_STYLE4 };
	private static final String NIH_INDEX = "NIH Index: Not Invented Here - How much do we re-invent what is already available and excellent.";
	private static final String NIH_INDEX1 = "Nothing is good enough. Easier to re-invent our own frameworks, thank you very much.";
	private static final String NIH_INDEX2 = "Depends on the group involved, there is no consistency.";
	private static final String NIH_INDEX3 = "We always practice 'Proudly Invented Elsewhere' wherever reasonable";
	private static final String NIH_INDEX4 = "If it doesn't come shrink wrapped and commercially supported, we don't use it";
	private static final String[] NIH_INDEX_OPTIONS = { NIH_INDEX1, NIH_INDEX2,
			NIH_INDEX3, NIH_INDEX4 };
	private static final String PERSISTENCE_INDEX = "Persistence Flexibility";
	private static final String PERSISTENCE_INDEX1 = "We persist everything in flat files";
	private static final String PERSISTENCE_INDEX2 = "We use Oracle for everything";
	private static final String PERSISTENCE_INDEX3 = "We use a mix of relational databases (SQL)";
	private static final String PERSISTENCE_INDEX4 = "We are experimenting with some NoSQL";
	private static final String PERSISTENCE_INDEX5 = "We use a sophisticated, fearless mix of whatever the best technology is for the purpose. Hadoop, Cassandra, you name it";
	private static final String[] PERSISTENCE_INDEX_OPTIONS = {
			PERSISTENCE_INDEX1, PERSISTENCE_INDEX2, PERSISTENCE_INDEX3,
			PERSISTENCE_INDEX4, PERSISTENCE_INDEX5 };
	private static final String FUNDING_TIGHTLOOSE = "Funding: Tight vs Loose";
	private static final String FUNDING_TIGHTLOOSE1 = "Our founder borrowed $50k from his grandmother last week.";
	private static final String FUNDING_TIGHTLOOSE2 = "We are established but not cash flush.";
	private static final String FUNDING_TIGHTLOOSE3 = "We can get the funding, if are willing to fight for it.";
	private static final String FUNDING_TIGHTLOOSE4 = "No problems here. Plenty-o-cash";
	private static final String[] FUNDING_TIGHTLOOSE_OPTIONS = {
			FUNDING_TIGHTLOOSE1, FUNDING_TIGHTLOOSE2, FUNDING_TIGHTLOOSE3,
			FUNDING_TIGHTLOOSE4 };
	private static final String BUDGETING_STYLE = "Budgeting Policies";
	private static final String BUDGETING_STYLE1 = "One dime over budget and the director is fired, on the spot.";
	private static final String BUDGETING_STYLE2 = "Things are serious. A budget is pretty much what you get to spend.";
	private static final String BUDGETING_STYLE3 = "Better not go over budget, you'll have to fight for extra cash if you need it.";
	private static final String BUDGETING_STYLE4 = "Just tell us if this isn't enough, we'll increase the budget as needed.";
	private static final String[] BUDGETING_STYLE_OPTIONS = { BUDGETING_STYLE1,
			BUDGETING_STYLE2, BUDGETING_STYLE3, BUDGETING_STYLE4 };
	private static final String STAFFING_STYLE = "Staffing Policies - Starvation vs Pile-On";
	private static final String STAFFING_STYLE1 = "We starve projects. Makes people work harder. Everyone is dying, which is good!";
	private static final String STAFFING_STYLE2 = "We run a tight ship. Do your best, we'll adjust if we have to.";
	private static final String STAFFING_STYLE3 = "Normal corporate Loose-Tight ambiguity";
	private static final String STAFFING_STYLE4 = "We'd love to give you more people on your team, if only you would let us.";
	private static final String[] STAFFING_STYLE_OPTIONS = { STAFFING_STYLE1,
			STAFFING_STYLE2, STAFFING_STYLE3, STAFFING_STYLE4 };
	private static final String ACCOUNTABILITY_INDEX = "Accountability Style";
	private static final String ACCOUNTABILITY_INDEX1 = "No one is held accountable for anything. People could get their feelings hurt.";
	private static final String ACCOUNTABILITY_INDEX2 = "It's a mixed bag here. Everyone genuinely wants to do their best.";
	private static final String ACCOUNTABILITY_INDEX3 = "It's a tough environment here. You are expected to get your stuff done, period.";
	private static final String ACCOUNTABILITY_INDEX4 = "Even if it takes 15 times, your code may not be committed until it passes rigorous, detailed code reviews.";
	private static final String[] ACCOUNTABILITY_INDEX_OPTIONS = {
			ACCOUNTABILITY_INDEX1, ACCOUNTABILITY_INDEX2,
			ACCOUNTABILITY_INDEX3, ACCOUNTABILITY_INDEX4 };
	private static final String CONSENSUS_STYLE = "Consensus Style";
	private static final String CONSENSUS_STYLE1 = "We had a meeting last year about something. Can't remember what.";
	private static final String CONSENSUS_STYLE2 = "Sometimes management suggests we have meetings to work out problems.";
	private static final String CONSENSUS_STYLE3 = "We regularly meet to review coming decisions, designs";
	private static final String CONSENSUS_STYLE4 = "Nothing proceeds forward without hours, days of meetings first";
	private static final String[] CONSENSUS_STYLE_OPTIONS = { CONSENSUS_STYLE1,
			CONSENSUS_STYLE2, CONSENSUS_STYLE3, CONSENSUS_STYLE4 };
	private static final String MARKET_STATUS = "Status of the Market for Technical Talent";
	private static final String MARKET_STATUS1 = "We have dozens of applicants for every opening.";
	private static final String MARKET_STATUS2 = "Things are in equilibrium. We can generally find good people, given the time.";
	private static final String MARKET_STATUS3 = "Things are tough here. We really have to grind to find good applicants.";
	private static final String MARKET_STATUS4 = "We have openings for months with no qualified applicants";
	private static final String[] MARKET_STATUS_OPTIONS = { MARKET_STATUS1,
			MARKET_STATUS2, MARKET_STATUS3, MARKET_STATUS4 };
	private static final String TASK_V_ROLES_STYLE = "Task vs Roles - How Much Do People Hide Behind Roles?";
	private static final String TASK_V_ROLES_STYLE1 = "If a person is at their desk, their role on this team is fulfilled.";
	private static final String TASK_V_ROLES_STYLE2 = "People get their job done, that is what is important.";
	private static final String TASK_V_ROLES_STYLE3 = "We're agile. Everyone is always tasked with specific responsibilities.";
	private static final String TASK_V_ROLES_STYLE4 = "There are no roles. We are a task based shop.";
	private static final String[] TASK_V_ROLES_STYLE_OPTIONS = {
			TASK_V_ROLES_STYLE1, TASK_V_ROLES_STYLE2, TASK_V_ROLES_STYLE3,
			TASK_V_ROLES_STYLE4 };
	private static final String REMOTE_STYLE = "Remote Workers: How much do people work remotely?";
	private static final String REMOTE_STYLE1 = "Not allowed";
	private static final String REMOTE_STYLE2 = "Some work remotely but most work from the office 100% of the time";
	private static final String REMOTE_STYLE3 = "Many people work remotely for some of the week.";
	private static final String REMOTE_STYLE4 = "We have no offices. Everyone works remotely.";
	private static final String[] REMOTE_STYLE_OPTIONS = { REMOTE_STYLE1,
			REMOTE_STYLE2, REMOTE_STYLE3, REMOTE_STYLE4 };
	private static final String OPERATIONAL_V_ANALYTIC = "Operational vs Analytic";
	private static final String OPERATIONAL_V_ANALYTIC1 = "This project is operational data. Everything is a transaction, every detail must be bulletproof.";
	private static final String OPERATIONAL_V_ANALYTIC2 = "This project is a mix of operational and analytic data, but mostly operational";
	private static final String OPERATIONAL_V_ANALYTIC3 = "This project is a mix of analytic and operational data, but mostly analytic";
	private static final String OPERATIONAL_V_ANALYTIC4 = "This project is analytical data. General trends are the big win. Algorithms are for speed, not accuracy.";
	private static final String[] OPERATIONAL_V_ANALYTIC_OPTIONS = {
			OPERATIONAL_V_ANALYTIC1, OPERATIONAL_V_ANALYTIC2,
			OPERATIONAL_V_ANALYTIC3, OPERATIONAL_V_ANALYTIC4 };
	private static final String SOFTWARE_LIFE = "Lifetime of this Software - How Long Will This Software Be Maintained";
	private static final String SOFTWARE_LIFE1 = "Who cares? Just get the damned prototype working, and quickly, too!";
	private static final String SOFTWARE_LIFE2 = "We're not sure, just do the best you can. We'll worry about that next year.";
	private static final String SOFTWARE_LIFE3 = "This is a 3-5 year project. Expect to maintain this for that period of time";
	private static final String SOFTWARE_LIFE4 = "This project is our company's lifeblood and will be maintained for decades.";
	private static final String[] SOFTWARE_LIFE_OPTIONS = { SOFTWARE_LIFE1,
			SOFTWARE_LIFE2, SOFTWARE_LIFE3, SOFTWARE_LIFE4 };
	private static final String PROJECT_BUDGET_MONTHS = "Project Budget Scope";
	private static final String PROJECT_BUDGET_MONTHS1 = "6 months - This schedule budgets for the first 6 months";
	private static final String PROJECT_BUDGET_MONTHS2 = "12 months - This schedule budgets for the first 12 months";
	private static final String PROJECT_BUDGET_MONTHS3 = "2 years - This schedule budgets for the first 2 years of the project";
	private static final String PROJECT_BUDGET_MONTHS4 = "5 years - This schedule budgets for the first 5 years of the project";
	private static final String[] PROJECT_BUDGET_MONTHS_OPTIONS = {
			PROJECT_BUDGET_MONTHS1, PROJECT_BUDGET_MONTHS2,
			PROJECT_BUDGET_MONTHS3, PROJECT_BUDGET_MONTHS4 };
	private static final String BUDGET_WINDOW = "Project Budget in Months: How many months is this project being budgeted for in this schedule?";
	private static final String BUDGET_WINDOW1 = "6 Months. Get it done in 6 months. Don't worry about anything after that.";
	private static final String BUDGET_WINDOW2 = "12 Months. Get it done in 12 months. Don't worry about anything after that";
	private static final String BUDGET_WINDOW3 = "1/5 Years. Get it done in 1 year. Maintain it for another 5 after that";
	private static final String BUDGET_WINDOW4 = "1/10 Years. Get it done in 1 year. Maintain it for another 10 after that";
	private static final String[] BUDGET_WINDOW_OPTIONS = { BUDGET_WINDOW1,
			BUDGET_WINDOW2, BUDGET_WINDOW3, BUDGET_WINDOW4 };
	private static final String DATA_SCALE = "Scale of Data - What are the data persistence server requirements?";
	private static final String DATA_SCALE1 = "Departmental - The data could fit in a single modestly sized server";
	private static final String DATA_SCALE2 = "Impressive - The data could fit in a single, honkingly large server";
	private static final String DATA_SCALE3 = "Clustered - The data would require multiple servers some kind of cluster arrangement";
	private static final String DATA_SCALE4 = "WebScale - Requires an infinitely expandable array of servers";
	private static final String[] DATA_SCALE_OPTIONS = { DATA_SCALE1,
			DATA_SCALE2, DATA_SCALE3, DATA_SCALE4 };
	private static final String OPERATIONS_SCALE = "Scale of Data Operations - Background processing and grinding of data";
	private static final String OPERATIONS_SCALE1 = "None - this project uses whatever data it is given.";
	private static final String OPERATIONS_SCALE2 = "Minimal - this project creates some materialized views, not much more than that.";
	private static final String OPERATIONS_SCALE3 = "Some - this project does some degree of background processing with the data received, before it can be consumed.";
	private static final String OPERATIONS_SCALE4 = "Extensive - this project requires extensive processing of data after it is received, and before it can be consumed.";
	private static final String[] OPERATIONS_SCALE_OPTIONS = {
			OPERATIONS_SCALE1, OPERATIONS_SCALE2, OPERATIONS_SCALE3,
			OPERATIONS_SCALE4 };
	private static final String REQUESTS_SCALE = "Scale of requests - public facing servers";
	private static final String REQUESTS_SCALE1 = "Thousands per hour, one medium size server should do";
	private static final String REQUESTS_SCALE2 = "Millions - need several servers";
	private static final String REQUESTS_SCALE3 = "Zillions - need a server farm";
	private static final String REQUESTS_SCALE4 = "QuadZillions - must scale up and down infinitely to meet demand";
	private static final String[] REQUESTS_SCALE_OPTIONS = { REQUESTS_SCALE1,
			REQUESTS_SCALE2, REQUESTS_SCALE3, REQUESTS_SCALE4 };
	private static final String ISOMORPHISM_AUTO = "Isomorphic Automation - Degree that team automates isomporphic code operations";
	private static final String ISOMORPHISM_AUTO1 = "What does isomorphic mean?";
	private static final String ISOMORPHISM_AUTO2 = "We use the IDE to automate some processes, which gives us a boost";
	private static final String ISOMORPHISM_AUTO3 = "We use grails, rails, hibernate, or similar tools on some projects";
	private static final String ISOMORPHISM_AUTO4 = "If it's isomporphic, we automate it. We train for this, internally.";
	private static final String[] ISOMORPHISM_AUTO_OPTIONS = {
			ISOMORPHISM_AUTO1, ISOMORPHISM_AUTO2, ISOMORPHISM_AUTO3,
			ISOMORPHISM_AUTO4 };
	private static final String HIRE_V_EXTEND = "Hiring vs Extend Schedule - What generator should do if inadequate resources are specified";
	private static final String HIRE_V_EXTEND1 = "Fail, abort";
	private static final String HIRE_V_EXTEND2 = "Extend schedule in time until tasks are complete";
	private static final String HIRE_V_EXTEND3 = "Hire new resources, adding the hiring itself as additional tasks";
	private static final String HIRE_V_EXTEND4 = "Hire new resources, making them appear magically without additional hiring tasks";
	private static final String[] HIRE_V_EXTEND_OPTIONS = { HIRE_V_EXTEND1, HIRE_V_EXTEND2,
			HIRE_V_EXTEND3, HIRE_V_EXTEND4 };
	private static final String API_REQUIRED = "API - Does this project require a portion of it's API to be exposed for direct access?";
	private static final String API_REQUIRED1 = "No";
	private static final String API_REQUIRED2 = "A small part of the API must be exposed";
	private static final String API_REQUIRED3 = "Almost all of the API must be exposed";
	private static final String API_REQUIRED4 = "The project takes no actions except through it's exposed API";
	private static final String[] API_REQUIRED_OPTIONS = { API_REQUIRED1, API_REQUIRED2,
			API_REQUIRED3, API_REQUIRED4 };
	private static final String INTERNAL_MESSAGING = "Internal Messaging";
	private static final String INTERNAL_MESSAGING1 = "All language based synchronous - Project uses internal language APIs to communicate internally, such as with the JVM";
	private static final String INTERNAL_MESSAGING2 = "Mixed, mostly synchronous - Hetorogenous messaging used to communicate internally, most of it is synchronous";
	private static final String INTERNAL_MESSAGING3 = "Aggressive asynchronous but mixed - Hetorogenous messaging trending towards aggressive on the asynchronous side.";
	private static final String INTERNAL_MESSAGING4 = "Asynchronous Homogenous - such as everything happens via a Kafka buffered message";
	private static final String[] INTERNAL_MESSAGING_OPTIONS = { INTERNAL_MESSAGING1,
			INTERNAL_MESSAGING2, INTERNAL_MESSAGING3, INTERNAL_MESSAGING4 };
	private static final String PROCESS_RIGOR = "Process Rigor";
	private static final String PROCESS_RIGOR1 = "People work. That is rigorous enough.";
	private static final String PROCESS_RIGOR2 = "We have our way of doing things. It is expected that one follows these ways";
	private static final String PROCESS_RIGOR3 = "Process taken seriously around here. For example, developers are expected to write regression tests.";
	private static final String PROCESS_RIGOR4 = "We follow a rigid ThoughtWorks like model. Pair programming, testing, CD, the works. No one gets to do it their way instead";
	private static final String[] FOUR_STYLE_OPTIONS = { PROCESS_RIGOR1,
			PROCESS_RIGOR2, PROCESS_RIGOR3, PROCESS_RIGOR4 };
	private static final String IMPLICIT_EXPLICIT = "Implicit vs Explicit Schemas (Schema On Read vs Schema on Write)";
	private static final String IMPLICIT_EXPLICIT1 = "We write code. I don't even know what you are talking about.";
	private static final String IMPLICIT_EXPLICIT2 = "Our relational database is our schema";
	private static final String IMPLICIT_EXPLICIT3 = "Mixed - we have some NoSQL and some of that is is Schema on Read";
	private static final String IMPLICIT_EXPLICIT4 = "Schema on Read - We use [Avro, other] for every piece of data anywhere.";
	private static final String[] IMPLICIT_EXPLICIT_OPTIONS = { IMPLICIT_EXPLICIT1,
			IMPLICIT_EXPLICIT2, IMPLICIT_EXPLICIT3, IMPLICIT_EXPLICIT4 };
	Map<String, ProjectFactor> projectFactors = new HashMap<String, ProjectFactor>();
	String description = "pleaseInsertDescriptionHere";

	void createComponents() {
		addSimple("howManyDevelopers", HOW_MANY_DEVELOPERS, "0");
		addSimple("howManyOpsEngineers", HOW_MANY_OPS_ENGINEERS, "0");
		addSimple("howManyQATesters", HOW_MANY_QA_TESTERS, "0");
		addSimple("howManyManagers", HOW_MANY_MANAGERS, "0");
		addSimple("howManyBAs", HOW_MANY_BA, "0");
		addSimple("howManyDBAs", HOW_MANY_DBA, "0");
		addSimple("howManyUIDesigners", HOW_MANY_UI_DESIGNERS, "0");
		addChoice("hiringStyle", HIRING_STYLE, HIRING_STYLE_OPTIONS, 1);
		addSimple("averageYearsExperience", AVERAGE_YEARS_EXPERIENCE, "5");
		addChoice("cultureType", CULTURE_TYPE, CULTURE_TYPE_OPTIONS, 1);
		addChoice("documentationStyle", DOCUMENTATION_STYLE, DOCUMENTATION_STYLE_OPTIONS, 1);
		addChoice("testingStyle", TESTING_STYLE, TESTING_STYLE_OPTIONS, 1);
		addChoice("cdStyle", CD_STYLE, CD_STYLE_OPTIONS, 1);
		addSimple("whichLanguagesThisProject", WHICH_LANGUAGES_THIS_PROJECT, "Java JavaScript");
		addChoice("qualityStyle", QUALITY_STYLE, QUALITY_STYLE_OPTIONS, 1);
		addRange("backFrontEndPercentage", BACK_FRONT_END, 0, 100, 50);
		addChoice("javaModularityStyle", JAVA_MODULARITY_STYLE, JAVA_MODULARITY_STYLE_OPTIONS, 1);
		addChoice("containerModularityStyle", CONTAINER_MODULARITY_STYLE,
				CONTAINER_MODULARITY_STYLE_OPTIONS, 1);
		addChoice("domainSampleSet", DOMAIN_SAMPLE_SET, DOMAIN_SAMPLE_SET_OPTIONS, 1);
		addRange("howManyTables", HOW_MANY_TABLES, 0, 100, 50);
		addRange("degreeDataCustomization", DEGREE_OF_DATA_CUSTOMIZATION, 0, 100, 50);
		addChoice("designStyle", DESIGN_STYLE, DESIGN_STYLE_OPTIONS, 1);
		addChoice("nihIndex", NIH_INDEX, NIH_INDEX_OPTIONS, 1);
		addChoice("persistenceIndex", PERSISTENCE_INDEX, PERSISTENCE_INDEX_OPTIONS, 1);
		addChoice("fundingTightLoose", FUNDING_TIGHTLOOSE, FUNDING_TIGHTLOOSE_OPTIONS, 1);
		addChoice("budgetingStyle", BUDGETING_STYLE, BUDGETING_STYLE_OPTIONS, 1);
		addChoice("staffingStyle", STAFFING_STYLE, STAFFING_STYLE_OPTIONS, 1);
		addChoice("accountabilityStyle", ACCOUNTABILITY_INDEX, ACCOUNTABILITY_INDEX_OPTIONS, 1);
		addChoice("consensusStyle", CONSENSUS_STYLE, CONSENSUS_STYLE_OPTIONS, 1);
		addChoice("marketStatus", MARKET_STATUS, MARKET_STATUS_OPTIONS, 1);
		addChoice("tasksVsRoles", TASK_V_ROLES_STYLE, TASK_V_ROLES_STYLE_OPTIONS, 1);
		addChoice("remoteStyle", REMOTE_STYLE, REMOTE_STYLE_OPTIONS, 1);
		addChoice("operationalStyle", OPERATIONAL_V_ANALYTIC, OPERATIONAL_V_ANALYTIC_OPTIONS, 1);
		addChoice("softwareLife", SOFTWARE_LIFE, SOFTWARE_LIFE_OPTIONS, 1);
		addChoice("projectBudgetMonths", PROJECT_BUDGET_MONTHS, PROJECT_BUDGET_MONTHS_OPTIONS, 1);
		addChoice("budgetWindow", BUDGET_WINDOW, BUDGET_WINDOW_OPTIONS, 1);
		addChoice("dataScale", DATA_SCALE, DATA_SCALE_OPTIONS, 1);
		addChoice("operationsScale", OPERATIONS_SCALE, OPERATIONS_SCALE_OPTIONS, 1);
		addChoice("requestsScale", REQUESTS_SCALE, REQUESTS_SCALE_OPTIONS, 1);
		addChoice("isomorphismAuto", ISOMORPHISM_AUTO, ISOMORPHISM_AUTO_OPTIONS, 1);
		addChoice("hireVsExtend", HIRE_V_EXTEND, HIRE_V_EXTEND_OPTIONS, 1);
		addChoice("apiRequired", API_REQUIRED, API_REQUIRED_OPTIONS, 1);
		addChoice("internalMessaging", INTERNAL_MESSAGING, INTERNAL_MESSAGING_OPTIONS, 1);
		addChoice("processRigor", PROCESS_RIGOR, FOUR_STYLE_OPTIONS, 1);
		addChoice("implicitExplicit", IMPLICIT_EXPLICIT, IMPLICIT_EXPLICIT_OPTIONS, 1);
	}
	
	void write(){
		ProjectProfile projectProfile = new ProjectProfile();
		projectProfile.setProjectFactors(projectFactors);
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(new File("src/main/resources/META-INF/config/projectProfile_z.json"),
            		projectProfile);
        } catch (JsonGenerationException e) {
            throw new RuntimeException(e);
        } catch (JsonMappingException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
	}

	void addSimple(String id, String name, String value) {
		projectFactors.put(id, new ProjectFactor(id, name, description, value));
	}

	void addChoice(String id, String name,  String[] choices, int choice) {
		projectFactors.put(id, new ProjectFactor(id, name, description, choices,
				choice));
	}

	void addRange(String id, String name,  int choice, int bottomRange,
			int topRange) {
		projectFactors.put(description, new ProjectFactor(id, name, description,
				choice, bottomRange, topRange));

	}

}
