package smoslt.generator.obsolete;

import java.io.File;
import java.util.SortedMap;
import java.util.TreeMap;

import com.fasterxml.jackson.databind.ObjectMapper;

import smoslt.domain.ProjectProfile;

public class DeleteMe {

	public static void main(String[] args) throws Exception{
		new DeleteMe().go();
	}

	private void go() throws Exception{
		ObjectMapper objectMapper = new ObjectMapper();
		ProjectProfile projectProfile = objectMapper.readValue(new File("src/main/resources/META-INF/config/projectProfile_a.json"),
				ProjectProfile.class);
		SortedMap<String, String> categories = new TreeMap<String, String>();
		categories.put("technologyBaseline","Technology Baseline: What is the baseline for this project and or our shop before considering options?");
		categories.put("sizeScale","Size, Scale: What kind of scale does this application and it's components have to reach?");
		categories.put("cultureBias","Cultural Bias: What is the nature of this team's cultural/habitual bias");
		categories.put("teamComposition","Team Composition: What is the composition of the team and how is it allowed to grow?");
		projectProfile.setCategories(categories);
		objectMapper.writerWithDefaultPrettyPrinter().writeValue(new File("src/main/resources/META-INF/config/projectProfile_z.json"),
        		projectProfile);
	}

}
