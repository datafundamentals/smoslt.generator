package smoslt.generator.obsolete;

import java.io.File;

import org.btrg.uti.StringUtils_;

import smoslt.domain.ProjectFactor;

public class GenerateHelpers {
	File dir = new File(
			"/Users/petecarapetyan/work/smoslt/smoslt.generator/src/main/java/smoslt/generator/helper/");
	String name;
	String constant;
	ProjectFactor projectFactor;

	public GenerateHelpers(ProjectFactor projectFactor) {
		this.projectFactor = projectFactor;
		this.name = StringUtils_.upStart(projectFactor.getId());
		this.constant = StringUtils_.camelCaseConvertToConstant(name);
	}

	public void go() {
		throw new RuntimeException();
		/*
		 * This over-writes all classes in package smoslt.generator.helper, so
		 * you don't want to run this without first directing it to a different
		 * package directory to save the old ones
		 */
		// File file = new File(dir, name+"Generator.java");
		// File sourceFile = new File(dir, "HowManyOpsEngineersGenerator.java");
		// String text = null;
		// try {
		// text = FileAsString_.read(sourceFile);
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// text = StringUtils_.replace(text, "HowManyOpsEngineersGenerator",
		// name+"Generator");
		// new FileWrite_(file.getAbsolutePath(), text);
	}

}
