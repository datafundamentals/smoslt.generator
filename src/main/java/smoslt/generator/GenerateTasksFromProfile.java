package smoslt.generator;

import smoslt.domain.Models;
import smoslt.domain.ProjectFactor;
import smoslt.domain.ProjectProfile;
import smoslt.domain.Schedule;
import smoslt.generator.helper.ApiRequiredGenerator;
import smoslt.generator.helper.CdStyleGenerator;
import smoslt.generator.helper.ConsensusStyleGenerator;
import smoslt.generator.helper.DataScaleGenerator;
import smoslt.generator.helper.DegreeDataCustomizationGenerator;
import smoslt.generator.helper.DesignStyleGenerator;
import smoslt.generator.helper.DocumentationStyleGenerator;
import smoslt.generator.helper.DomainTableNamesGenerator;
import smoslt.generator.helper.InternalMessagingGenerator;
import smoslt.generator.helper.IsomorphismAutoGenerator;
import smoslt.generator.helper.NihIndexGenerator;
import smoslt.generator.helper.OperationsScaleGenerator;
import smoslt.generator.helper.PersistenceIndexGenerator;
import smoslt.generator.helper.RequestsScaleGenerator;
import smoslt.generator.helper.SoftwareLifeGenerator;
import smoslt.generator.helper.SoftwareProcessesGenerator;
import smoslt.generator.helper.TestingStyleGenerator;

public class GenerateTasksFromProfile {
	ProjectProfile projectProfile;
	private Schedule schedule;

	public GenerateTasksFromProfile(ProjectProfile projectProfile,
			Schedule schedule) {
		this.projectProfile = projectProfile;
		this.schedule = schedule;
	}

	public void execute() {
		for (String phase : Models.getPhases()) {
			for (ProjectFactor projectFactor : projectProfile
					.getProjectFactors().values()) {
				switch (projectFactor.getId()) {
				case "domainTableNames":
					new DomainTableNamesGenerator(projectFactor,
							projectProfile, schedule, phase);
					break;
				case "softwareProcesses":
					new SoftwareProcessesGenerator(projectFactor,
							projectProfile, schedule, phase);
					break;
				case "isomorphismAuto":
					new IsomorphismAutoGenerator(projectFactor, projectProfile,
							schedule, phase);
					break;
				case "designStyle":
					new DesignStyleGenerator(projectFactor, projectProfile,
							schedule, phase);
					break;
				case "nihIndex":
					new NihIndexGenerator(projectFactor, projectProfile,
							schedule, phase);
					break;
				case "consensusStyle":
					new ConsensusStyleGenerator(projectFactor, projectProfile,
							schedule, phase);
					break;
				case "cdStyle":
					new CdStyleGenerator(projectFactor, projectProfile,
							schedule, phase);
					break;
				case "documentationStyle":
					new DocumentationStyleGenerator(projectFactor,
							projectProfile, schedule, phase);
					break;
				case "testingStyle":
					new TestingStyleGenerator(projectFactor, projectProfile,
							schedule, phase);
					break;
				case "persistenceIndex":
					new PersistenceIndexGenerator(projectFactor,
							projectProfile, schedule, phase);
					break;
				case "internalMessaging":
					new InternalMessagingGenerator(projectFactor,
							projectProfile, schedule, phase);
					break;
				case "apiRequired":
					new ApiRequiredGenerator(projectFactor, projectProfile,
							schedule, phase);
					break;
				case "softwareLife":
					new SoftwareLifeGenerator(projectFactor, projectProfile,
							schedule, phase);
					break;
				case "requestsScale":
					new RequestsScaleGenerator(projectFactor, projectProfile,
							schedule, phase);
					break;
				case "dataScale":
					new DataScaleGenerator(projectFactor, projectProfile,
							schedule, phase);
					break;
				case "operationsScale":
					new OperationsScaleGenerator(projectFactor, projectProfile,
							schedule, phase);
					break;
				case "degreeDataCustomization":
					new DegreeDataCustomizationGenerator(projectFactor,
							projectProfile, schedule, phase);
					break;
				}
			}
		}
		schedule.setProjectProfile(projectProfile);
	}
}

// case "howManyOpsEngineers":
// new HowManyOpsEngineersGenerator(projectFactor,
// projectProfile, schedule, phase);
// break;
// case "howManyDevelopers":
// new HowManyDevelopersGenerator(projectFactor,
// projectProfile, schedule, phase);
// break;
// case "howManyManagers":
// new HowManyManagersGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "howManyUIDesigners":
// new HowManyUIDesignersGenerator(projectFactor,
// projectProfile, schedule, phase);
// break;
// case "howManyQATesters":
// new HowManyQATestersGenerator(projectFactor,
// projectProfile, schedule, phase);
// break;
// case "howManyBAs":
// new HowManyBAsGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "howManyDBAs":
// new HowManyDBAsGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "averageYearsExperience":
// new AverageYearsExperienceGenerator(projectFactor,
// projectProfile, schedule, phase);
// break;
// case "hiringStyle":
// new HiringStyleGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "hireVsExtend":
// new HireVsExtendGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "marketStatus":
// new MarketStatusGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "staffingStyle":
// new StaffingStyleGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "implicitExplicit":
// new ImplicitExplicitGenerator(projectFactor,
// projectProfile, schedule, phase);
// break;
// case "domainSampleSet":
// new DomainSampleSetGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "domainTableNames":
// new DomainTableNamesGenerator(projectFactor,
// projectProfile, schedule, phase);
// break;
// case "domainModelCount":
// new DomainModelCountGenerator(projectFactor,
// projectProfile, schedule, phase);
// break;
// case "softwareProcesses":
// new SoftwareProcessesGenerator(projectFactor,
// projectProfile, schedule, phase);
// break;
// case "operationalStyle":
// new OperationalStyleGenerator(projectFactor,
// projectProfile, schedule, phase);
// break;
// case "accountabilityStyle":
// new AccountabilityStyleGenerator(projectFactor,
// projectProfile, schedule, phase);
// break;
// case "agileDefinedAs":
// new AgileDefinedAsGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "isomorphismAuto":
// new IsomorphismAutoGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "fundingTightLoose":
// new FundingTightLooseGenerator(projectFactor,
// projectProfile, schedule, phase);
// break;
// case "qualityStyle":
// new QualityStyleGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "cultureType":
// new CultureTypeGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "remoteStyle":
// new RemoteStyleGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "processRigor":
// new ProcessRigorGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "designStyle":
// new DesignStyleGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "nihIndex":
// new NihIndexGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "consensusStyle":
// new ConsensusStyleGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "cdStyle":
// new CdStyleGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "tasksVsRoles":
// new TasksVsRolesGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "budgetingStyle":
// new BudgetingStyleGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "documentationStyle":
// new DocumentationStyleGenerator(projectFactor,
// projectProfile, schedule, phase);
// break;
// case "whichLanguagesThisProject":
// new WhichLanguagesThisProjectGenerator(projectFactor,
// projectProfile, schedule, phase);
// break;
// case "testingStyle":
// new TestingStyleGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "persistenceIndex":
// new PersistenceIndexGenerator(projectFactor,
// projectProfile, schedule, phase);
// break;
// case "containerModularityStyle":
// new ContainerModularityStyleGenerator(projectFactor,
// projectProfile, schedule, phase);
// break;
// case "javaModularityStyle":
// new JavaModularityStyleGenerator(projectFactor,
// projectProfile, schedule, phase);
// break;
// case "internalMessaging":
// new InternalMessagingGenerator(projectFactor,
// projectProfile, schedule, phase);
// break;
// case "budgetWindow":
// new BudgetWindowGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "apiRequired":
// new ApiRequiredGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "softwareLife":
// new SoftwareLifeGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "requestsScale":
// new RequestsScaleGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "dataScale":
// new DataScaleGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "projectBudgetMonths":
// new ProjectBudgetMonthsGenerator(projectFactor,
// projectProfile, schedule, phase);
// break;
// case "operationsScale":
// new OperationsScaleGenerator(projectFactor, projectProfile,
// schedule, phase);
// break;
// case "degreeDataCustomization":
// new DegreeDataCustomizationGenerator(projectFactor, projectProfile, schedule,
// phase); break;

