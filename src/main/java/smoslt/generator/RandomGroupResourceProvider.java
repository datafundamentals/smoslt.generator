package smoslt.generator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.btrg.utils.random.RandomProvider;

import smoslt.domain.Resource;

public class RandomGroupResourceProvider {
	private Map<String, List<String>> groupResources = new LinkedHashMap<String, List<String>>();
	private List<String> groups = new ArrayList<String>();
	private Map<String, Integer> favoriteResources = new HashMap<String, Integer>();

	public RandomGroupResourceProvider() {
		createNewGroups();
		createResources();
//		 printConstructor();
	}
	
	public Set<Resource> getResources(){
		Set<Resource> resources = new HashSet<Resource>();
		for (String group : groups) {
			List<String> resourceNames = groupResources.get(group);
			int i = 0;
			for (String resourceName : resourceNames) {
				Resource resource = new Resource(
						resourceName, group);
				resources.add(resource);
			}
		}
		return resources;
	}

	private void printConstructor() {
		for (String group : groups) {
			System.out.println(group + " GROUP");
			List<String> resources = groupResources.get(group);
			for (String resource : resources) {
				System.out.print("\t" + resource);
				if (favoriteResources.containsKey(resource)) {
					System.out
							.println(" by " + favoriteResources.get(resource));
				} else {
					System.out.println();
				}
			}
		}
	}

	private void markFavoriteResources(String name) {
		if (RandomProvider.weightedToYesBoolean(10)) {
			int random = RandomProvider.random0toMax(10);
			if (random > 0) {
				favoriteResources.put(name, random);
			}
		}
	}

	private void createResources() {
		for (String groupName : groups) {
			int count = RandomProvider.weighted1through10(5, 10, 10, 20, 20, 5,
					15, 5, 5, 5);
			while (groupResources.get(groupName).size() < count) {
				String resourceName = RandomProvider.randomName();
				if (!groupsContain(resourceName)) {
					putInGroup(groupName, resourceName);
					markFavoriteResources(resourceName);
				}
			}
		}

	}

	private void putInGroup(String groupName, String resourceName) {
		List<String> resources = groupResources.get(groupName);
		if (!resources.contains(resourceName)) {
			resources.add(resourceName);
		}
	}

	private boolean groupsContain(String name) {
		boolean contains = false;
		for (String group : groups) {
			if (groupResources.get(group).contains(name)) {
				contains = true;
				break;
			}
		}
		return contains;
	}

	private void createNewGroups() {
		int count = 1 + RandomProvider.weighted1or2or3(30, 60, 10);
		while (groups.size() < count) {
			String groupName = RandomProvider.weightedGroup(50, 30, 10, 10);
			if (!groups.contains(groupName)) {
				groups.add(groupName);
				groupResources.put(groupName, new ArrayList<String>());
			}
		}
	}

	/*
	 * 
	 * this really needs to be weighted to a variation of what came before it on
	 * some types of projects, to make it more realistic on something like a
	 * software team
	 */
	public String getRandomResourcesListing() {
		StringBuffer sb = new StringBuffer();
		int groupCount = RandomProvider.weighted1or2or3(80, 10, 10);
		if (groupCount > groups.size()) {
			groupCount = groups.size();
		}
		Map<String, Integer> chosenGroups = new HashMap<String, Integer>();
		while (groupCount > 0 && groupCount > chosenGroups.size()) {
			String groupName = getRandomGroup();
			int maxSize = groupResources.get(groupName).size();
			if (!chosenGroups.containsKey(groupName)) {
				groupCount--;
				for (String resource : groupResources.get(groupName)) {
					if (favoriteResources.containsKey(resource)) {
						int value = favoriteResources.get(resource);
						boolean include = RandomProvider
								.weightedToYesBoolean(value / 2);
						if (include) {
							sb.append(resource + ", ");
							maxSize--;
						}
					}
				}
				if (maxSize > 0) {
					int thisSize = RandomProvider.random1toMax(maxSize);
					chosenGroups.put(groupName, thisSize);
				}
			}
		}
		for (String chosenGroup : chosenGroups.keySet()) {
			sb.append(chosenGroups.get(chosenGroup) + " " + chosenGroup + ", ");
		}
		String returnValue = sb.toString().trim();
		if (returnValue.endsWith(",")) {
			returnValue = returnValue.substring(0, returnValue.length() - 1);
		}
		if (returnValue.trim().length() == 0) {
			returnValue = getRandomResourcesListing();
		}
		return returnValue;
	}

	public String getRandomGroup() {
		int whichOne = RandomProvider.random0toMax(groups.size() - 1);
		return groups.get(whichOne);
	}

	public int getGroupSize() {
		int groupKeySize = groupResources.keySet().size();
		int groupSize = groups.size();
		if (groupKeySize != groupSize) {
			throw new IllegalStateException();
		}
		return groupSize;
	}
}
