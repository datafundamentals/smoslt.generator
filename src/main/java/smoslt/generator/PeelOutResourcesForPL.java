package smoslt.generator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.btrg.utils.random.RandomProvider;

import smoslt.domain.Models;
import smoslt.domain.ProjectProfile;
import smoslt.domain.Resource;
import smoslt.domain.ScheduleBuild;
import smoslt.domain.Task;

public class PeelOutResourcesForPL {
	ProjectProfile projectProfile;
	private List<Task> taskList = new ArrayList<Task>();
	private Set<Resource> resources = new HashSet<Resource>();

	public PeelOutResourcesForPL(ProjectProfile projectProfile,
			ScheduleBuild scheduleBuild) {
		this.projectProfile = projectProfile;
		this.resources = scheduleBuild.getResources();
		this.taskList = scheduleBuild.getTasks();
	}

	public void execute() {
		for (String id : projectProfile.getProjectFactors().keySet()) {
			switch (id) {
			case "howManyManagers":
				addResource(Models.RESOURCE_PROJ_MGR, projectProfile
						.getProjectFactors().get(id).getValue());
				break;
			case "howManyOpsEngineers":
				addResource(Models.RESOURCE_OPS, projectProfile
						.getProjectFactors().get(id).getValue());
				break;
			case "howManyDevelopers":
				addResource(Models.RESOURCE_DEV_IMPL, projectProfile
						.getProjectFactors().get(id).getValue());
				break;
			case "howManyUIDesigners":
				addResource(Models.RESOURCE_DEV_DESIGN, projectProfile
						.getProjectFactors().get(id).getValue());
				break;
			case "howManyQATesters":
				addResource(Models.RESOURCE_QA_TEST, projectProfile
						.getProjectFactors().get(id).getValue());
				break;
			case "howManyDBAs":
				addResource(Models.RESOURCE_DBA, projectProfile
						.getProjectFactors().get(id).getValue());
				break;
			case "howManyBAs":
				addResource(Models.RESOURCE_BUS_ANLST, projectProfile
						.getProjectFactors().get(id).getValue());
				break;
			}
		}
	}

	private void addResource(String group, String value) {
		int count = Integer.valueOf(value);
		for (int i = 0; i < count; i++) {
			Resource resource = new Resource(RandomProvider.randomName(), group);
			resources.add(resource);
			if (i > 0) {
				Resource resourceDescriptor = new Resource(
						"" + i + " " + group, group);
				resources.add(resourceDescriptor);
			}
		}
	}

}
