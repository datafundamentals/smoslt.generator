package smoslt.generator;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import smoslt.domain.ProjectProfile;

public class ProjectProfileFactory {

	static ProjectProfileFactory instance = new ProjectProfileFactory();

	public static ProjectProfile get(File file) {
		return instance.execute(file);
	}

	private ProjectProfile execute(File file) {
		ProjectProfile projectProfile = null;
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			projectProfile = objectMapper.readValue(file, ProjectProfile.class);
		} catch (JsonParseException e) {
			throw new RuntimeException(e);
		} catch (JsonMappingException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return projectProfile;

	}
}
