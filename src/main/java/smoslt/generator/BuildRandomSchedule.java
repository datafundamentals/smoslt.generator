package smoslt.generator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import smoslt.domain.Resource;
import smoslt.domain.ScheduleBuild;
import smoslt.domain.Task;

/*
 * Unmaintainable spaghetti code creates bogus schedules for testing only. Not intended to be well written, etc. 
 */
public class BuildRandomSchedule implements ScheduleBuild {
	private static final int MAX_NUMBER_TASKS = 50;
	private static final int MAX_NUMBER_RESOURCES = 10;
	private static final int MAX_PERCENT_PREDECESSORS = 100;
	private static final int MAX_DAYS_PER_TASK = 10;
	public static final int MIN_NUMBER_TASKS = 10;
	public static final int MIN_NUMBER_RESOURCES = 4;
	public static final int MIN_PERCENT_PREDECESSORS = 80;
	public static final int MIN_DAYS_PER_TASK = 1;
	private int maxNumberTasksSeed;
	private int maxNumberResourcesSeed;
	private int maxPercentPredecessorsSeed;
	private int maxDaysPerTaskSeed;
	private List<Task> tasks;
	private List<Integer> predecessorIndex;
	private List<Task> noPredecessorsList = new ArrayList<Task>();
	private Set<Resource> resources = new HashSet<Resource>();
	private RandomGroupResourceProvider randomGroupResourceProvider = new RandomGroupResourceProvider();

	public static void main(String[] args) {
		BuildRandomSchedule build = new BuildRandomSchedule();
	}

	private void createTasks() {
		tasks = new ArrayList<Task>();
		for (int i = 0; i < getRandomRange(MIN_NUMBER_TASKS, maxNumberTasksSeed); i++) {
			int daysPerTask = getRandomRange(MIN_DAYS_PER_TASK,
					maxDaysPerTaskSeed);
//			Task task = new Task(daysPerTask, "" + (i + 1),
//					randomGroupResourceProvider.getRandomResourcesListing());
			Task task = new Task(daysPerTask, 0, "FIXME",  randomGroupResourceProvider.getRandomResourcesListing());
			tasks.add(task);
		}
	}

	public BuildRandomSchedule() {
		resources = randomGroupResourceProvider.getResources();
		setSeeds();
		createTasks();
		createPredecessorIndex();
		addPredecessors();
	}

	@Override
	public List<Task> getTasks() {
		return tasks;
	}

	@Override
	public Set<Resource> getResources() {
		return resources;
	}


	private void addPredecessors() {
		int j = 0;
		int k = 0;
		/*
		 * always adds the previous one, if it's in the predecessorIndex
		 */
		for (int i = 0; i < tasks.size(); i++) {
			k = (int) ((i - j) / 2);
			Task thisTask = tasks.get(i);
			if (i > 0 && predecessorIndex.contains(i - 1)) {
				thisTask.addPredecessor(tasks.get(i - 1));
			} else {
				j = i;
				Task taskToAdd = tasks.get(i - k);
				if (k > 1 && null != taskToAdd) {
					thisTask.addPredecessor(taskToAdd);
				}
				noPredecessorsList.add(thisTask);
			}
		}
	}

	private void createPredecessorIndex() {
		predecessorIndex = new ArrayList<Integer>();
		for (int i = 0; i < tasks.size(); i++) {
			if (addThisOne(i)) {
				predecessorIndex.add(i);
			}
		}
	}

	private boolean addThisOne(int i) {
		boolean addThisOne = false;
		int dividingLine = maxPercentPredecessorsSeed;
		int comparision = (int) (Math.random() * 100);
		if (comparision < dividingLine) {
			addThisOne = true;
		}
		return addThisOne;
	}

	private void setSeeds() {
		maxNumberTasksSeed = getRandomRange(MIN_NUMBER_TASKS, MAX_NUMBER_TASKS);
		maxNumberResourcesSeed = getRandomRange(MIN_NUMBER_RESOURCES,
				MAX_NUMBER_RESOURCES);
		maxPercentPredecessorsSeed = getRandomRange(MIN_PERCENT_PREDECESSORS,
				MAX_PERCENT_PREDECESSORS);
		maxDaysPerTaskSeed = getRandomRange(MIN_DAYS_PER_TASK,
				MAX_DAYS_PER_TASK);
	}

	private int getRandomRange(int low, int high) {
		if (high < low) {
			throw new RuntimeException();
		}
		if (high == low) {
			return low;
		}
		int diff = high - low;
		int random = (int) (Math.random() * diff);
		random = random + low;
		if (random > high) {
			return high;
		} else if (random < low) {
			return low;
		} else {
			return random;
		}
	}

}
