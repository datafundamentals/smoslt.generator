package smoslt.generator;

import smoslt.domain.Resource;
import smoslt.domain.Schedule;
import smoslt.domain.Task;

public class AssignTasks {
	private Schedule schedule;
	
	public AssignTasks(Schedule schedule){
		this.schedule = schedule;
	}

	public void execute() {
		for(Task task:schedule.getTaskList()){
			makeAssignments(task);
		}
	}

	private void makeAssignments(Task task) {
			for(String resourceRequirement:task.getResourceSpecifications()){
				for(Resource resource :schedule.getResources()){
					if(resource.getName().equals(resourceRequirement.trim())){
						schedule.addAssignment(task.getId(), resource.getId());	
						break;
					}
				}
			}
	}


}
