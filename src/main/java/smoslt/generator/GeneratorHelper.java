package smoslt.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import org.btrg.utils.random.RandomProvider;

import smoslt.domain.ProjectFactor;
import smoslt.domain.ProjectFactorType;
import smoslt.domain.ProjectProfile;
import smoslt.domain.Resource;
import smoslt.domain.Schedule;
import smoslt.domain.Task;

public class GeneratorHelper {
	public static final String PF_HOW_MANY_OPS_ENGINEERS = "howManyOpsEngineers";
	public static final String PF_HOW_MANY_DEVELOPERS = "howManyDevelopers";
	public static final String PF_HOW_MANY_MANAGERS = "howManyManagers";
	public static final String PF_HOW_MANY_U_I_DESIGNERS = "howManyUIDesigners";
	public static final String PF_HOW_MANY_Q_A_TESTERS = "howManyQATesters";
	public static final String PF_HOW_MANY_B_AS = "howManyBAs";
	public static final String PF_HOW_MANY_D_B_AS = "howManyDBAs";
	public static final String PF_AVERAGE_YEARS_EXPERIENCE = "averageYearsExperience";
	public static final String PF_HIRING_STYLE = "hiringStyle";
	public static final String PF_HIRE_VS_EXTEND = "hireVsExtend";
	public static final String PF_MARKET_STATUS = "marketStatus";
	public static final String PF_STAFFING_STYLE = "staffingStyle";
	public static final String PF_IMPLICIT_EXPLICIT = "implicitExplicit";
	public static final String PF_DOMAIN_SAMPLE_SET = "domainSampleSet";
	public static final String PF_DOMAIN_TABLE_NAMES = "domainTableNames";
	public static final String PF_DOMAIN_MODEL_COUNT = "domainModelCount";
	public static final String PF_SOFTWARE_PROCESSES = "softwareProcesses";
	public static final String PF_OPERATIONAL_STYLE = "operationalStyle";
	public static final String PF_ACCOUNTABILITY_STYLE = "accountabilityStyle";
	public static final String PF_AGILE_DEFINED_AS = "agileDefinedAs";
	public static final String PF_ISOMORPHISM_AUTO = "isomorphismAuto";
	public static final String PF_FUNDING_TIGHT_LOOSE = "fundingTightLoose";
	public static final String PF_QUALITY_STYLE = "qualityStyle";
	public static final String PF_CULTURE_TYPE = "cultureType";
	public static final String PF_REMOTE_STYLE = "remoteStyle";
	public static final String PF_PROCESS_RIGOR = "processRigor";
	public static final String PF_DESIGN_STYLE = "designStyle";
	public static final String PF_NIH_INDEX = "nihIndex";
	public static final String PF_CONSENSUS_STYLE = "consensusStyle";
	public static final String PF_CD_STYLE = "cdStyle";
	public static final String PF_TASKS_VS_ROLES = "tasksVsRoles";
	public static final String PF_BUDGETING_STYLE = "budgetingStyle";
	public static final String PF_DOCUMENTATION_STYLE = "documentationStyle";
	public static final String PF_WHICH_LANGUAGES_THIS_PROJECT = "whichLanguagesThisProject";
	public static final String PF_TESTING_STYLE = "testingStyle";
	public static final String PF_PERSISTENCE_INDEX = "persistenceIndex";
	public static final String PF_CONTAINER_MODULARITY_STYLE = "containerModularityStyle";
	public static final String PF_JAVA_MODULARITY_STYLE = "javaModularityStyle";
	public static final String PF_INTERNAL_MESSAGING = "internalMessaging";
	public static final String PF_BUDGET_WINDOW = "budgetWindow";
	public static final String PF_API_REQUIRED = "apiRequired";
	public static final String PF_SOFTWARE_LIFE = "softwareLife";
	public static final String PF_REQUESTS_SCALE = "requestsScale";
	public static final String PF_DATA_SCALE = "dataScale";
	public static final String PF_PROJECT_BUDGET_MONTHS = "projectBudgetMonths";
	public static final String PF_OPERATIONS_SCALE = "operationsScale";
	public static final String PF_DEGREE_DATA_CUSTOMIZATION = "degreeDataCustomization";
	public static final String PF_TESTING_TEAM = "testingTeam";
	public static final String PF_DATA_DESIGN = "dataDesign";
	public static final String PF_UI_DESIGN = "uiDesign";
	// public static final String PF_ = ""; template
	public static final String PHASE0_INITIATION = "Initiation";
	public static final String PHASE1_SYSTEM_CONCEPT_DEVELOPMENT = "System Concept Development";
	public static final String PHASE2_PLANNING = "Planning";
	public static final String PHASE3_REQUIREMENTS_ANALYSIS = "Requirements Analysis";
	public static final String PHASE4_DESIGN = "Design";
	public static final String PHASE5_DEVELOPMENT = "Development";
	public static final String PHASE6_INTEGRATION_TEST = "Integeration Testing";
	public static final String PHASE7_IMPLEMENTATION = "Implementation";
	public static final String PHASE8_OPERATIONS_MAINTENANCE = "Operations Maintenance";
	public static final String PHASE9_DISPOSITION = "Disposition";
	protected ProjectFactor projectFactor;
	protected ProjectProfile projectProfile;
	protected Schedule schedule;
	protected List<Task> taskList;
	protected Set<Resource> resources;
	protected String phase;
	private static List<String> modelNames = null;

	protected boolean phase0init(String phase) {
		return phase.equals(PHASE0_INITIATION);
	}

	protected boolean phase1concept(String phase) {
		return phase.equals(PHASE1_SYSTEM_CONCEPT_DEVELOPMENT);
	}

	protected boolean phase2planning(String phase) {
		return phase.equals(PHASE2_PLANNING);
	}

	protected boolean phase3reqs(String phase) {
		return phase.equals(PHASE3_REQUIREMENTS_ANALYSIS);
	}

	protected boolean phase4design(String phase) {
		return phase.equals(PHASE4_DESIGN);
	}

	protected boolean phase5dev(String phase) {
		return phase.equals(PHASE5_DEVELOPMENT);
	}

	protected boolean phase6test(String phase) {
		return phase.equals(PHASE6_INTEGRATION_TEST);
	}

	protected boolean phase7impl(String phase) {
		return phase.equals(PHASE7_IMPLEMENTATION);
	}

	protected boolean phase8maint(String phase) {
		return phase.equals(PHASE8_OPERATIONS_MAINTENANCE);
	}

	protected boolean phase9ops(String phase) {
		return phase.equals(PHASE9_DISPOSITION);
	}

	protected void addTask(int durationHours, int startDay, String description,
			int howMany, String groupRole) {
		Task newTask = new Task(durationHours, startDay, groupRole + ": "
				+ description, +howMany + " " + groupRole);
		taskList.add(newTask);
		Resource resource = getResource(howMany + " " + groupRole);
		if (resource != null) {
			schedule.addAssignment(newTask.getId(), resource.getId());
		}
	}

	private Resource getResource(String string) {
		Resource returnResource = null;
		for (Resource resource : resources) {
			if (string.equals(resource.getName())) {
				returnResource = resource;
				break;
			}
		}
		return returnResource;
	}

	protected List<String> getModels() {
		if (modelNames == null) {
			ProjectFactor projectFactor = projectProfile.getProjectFactors()
					.get("domainTableNames");
			modelNames = new ArrayList<String>();
			StringTokenizer stk = new StringTokenizer(projectFactor.getText(),
					"\n");
			while (stk.hasMoreTokens()) {
				modelNames.add(stk.nextToken().trim());
			}
		}
		return modelNames;
	}

	protected boolean eq(String projectFactorId, int value) {
		boolean eq = false;
		if (getProjectFactor(projectFactorId).getChoice() == value) {
			eq = true;
		}
		return eq;
	}

	protected boolean lt(String projectFactorId, int value) {
		boolean lt = false;
		if (getProjectFactor(projectFactorId).getChoice() < value) {
			lt = true;
		}
		return lt;
	}

	protected boolean gt(String projectFactorId, int value) {
		boolean gt = false;
		if (getProjectFactor(projectFactorId).getChoice() > value) {
			gt = true;
		}
		return gt;
	}

	protected boolean eqlt(String projectFactorId, int value) {
		boolean eqlt = false;
		if (getProjectFactor(projectFactorId).getChoice() <= value) {
			eqlt = true;
		}
		return eqlt;
	}

	protected boolean eqgt(String projectFactorId, int value) {
		boolean eqgt = false;
		if (getProjectFactor(projectFactorId).getChoice() >= value) {
			eqgt = true;
		}
		return eqgt;
	}

	private ProjectFactor getProjectFactor(String projectFactorId) {
		ProjectFactor otherProjectFactor = projectProfile.getProjectFactors()
				.get(projectFactorId);
		if (null == otherProjectFactor) {
			throw new IllegalArgumentException(projectFactorId
					+ " is not a projectFactorId");
		}
		if (otherProjectFactor.deduceProjectFactorType() != ProjectFactorType.MultipleChoice) {
			throw new IllegalArgumentException(projectFactorId
					+ " is not a multiple choice projectFactor");
		}
		return otherProjectFactor;
	}

	private int percentageOfHoursToDate(String role, int percentage) {
		// if(percentage>100){
		// throw new IllegalArgumentException("Cannot have a percentage of "+
		// percentage + "% because that is greater than 100%");
		// }
		// if(!Models.RESOURCE_ROLES.contains(role)){
		// throw new IllegalArgumentException("Resource role of '"+ role +
		// "' does not exist");
		// }
		// int hourForThisRoleToDate = 0;
		int returnResult = 0;
		// for(Task task:taskList){
		// for(String resourceRequirements:task.getResourceSpecifications()){
		/*
		 * rest of this code is in the Schedule class, but you can do that later
		 */
		// for(String resourceRequirement:resourceRequirements)
		// }
		// }
		// if(hourForThisRoleToDate>0){
		// returnResult= hourForThisRoleToDate/100*percentage;
		// }
		if (true) {
			throw new IllegalArgumentException();
		}
		return returnResult;
	}

	public boolean perhaps(int percentageYes) {
		return RandomProvider.weightedToYesBoolean(percentageYes);
	}

}
