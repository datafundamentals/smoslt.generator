package smoslt.generator.print;

import smoslt.domain.ResourceAvailability;
import smoslt.stacker.ResourceAvailabilities;

public class PrintResourceAvailabilities {

	public static void go(ResourceAvailabilities resourceAvailabilities) {
		System.out.println("RESOURCE AVAILABILITY");
		for(int i=0;i<resourceAvailabilities.getResourceAvailabilityCount();i++){
			ResourceAvailability ra = resourceAvailabilities.get(i);
			System.out.println(ra.getFirstDayAvailable() + " "+ra.getDaysDuration() + " " + " "+ra.getUnUsedDaysPreceding() + " " + " "+ra.getUnUsedDaysAfter() + " " + " "+ra.getResource().getName()+ " " + " "+ra.getResource().getGroup() );
		}
	}

}
