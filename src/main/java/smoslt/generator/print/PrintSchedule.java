package smoslt.generator.print;

import smoslt.domain.Schedule;
import smoslt.domain.Task;

public class PrintSchedule {

	public static void go(Schedule schedule) {
		System.out.println("SCHEDULE:");
		for (Task task : schedule.getTaskList()) {
			printTask(task);
		}
	}

	private static void printTask(Task task) {
		printDashes(task.getDurationDays());
		System.out.println();
	}

	private static void printSpace(int count) {
		printRepeat(count, ' ');
	}

	private static void printDashes(int count) {
		printRepeat(count, '-');
	}

	private static void printRepeat(int count, char charachter) {
		int i = count;
		while (i > 0) {
			System.out.print(charachter);
			i--;
		}
	}

}
