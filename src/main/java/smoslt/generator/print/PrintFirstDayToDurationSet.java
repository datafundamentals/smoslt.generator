package smoslt.generator.print;

import java.util.SortedMap;

public class PrintFirstDayToDurationSet {

	public static void go(SortedMap<Integer, Integer> firstDayToDuration) {
		System.out.println("FIRST DAY TO DURATION SET");
		for(int i:firstDayToDuration.keySet()){
			System.out.println(i + " " + firstDayToDuration.get(i));
		}
	}

}
