package smoslt.generator.print;

import java.util.Map;

import smoslt.stacker.ResourceReserver;

public class PrintResourceTablulators {

	public static void go(Map<Long, ResourceReserver> resourceReservers) {
		System.out
				.println("\nGRP\tNAME       \tId\t        RA_CNT  SPECD   RESRVD  1stDAY");
		for (ResourceReserver resourceReserver : resourceReservers.values()) {
			System.out.println(resourceReserver.getResource().getGroup()
					+ "\t"
					+ resourceReserver.getResource().getName()
					+ "\t"
					+ resourceReserver.getResource().getId()
					+ "\t"
					+ resourceReserver.getResourceAvailabilities()
							.getResourceAvailabilityCount()
					+ "\t"
					+ resourceReserver.isSpecified()
					+ "\t"
					+ resourceReserver.isReservedForThisTask()
					+ "\t"
					+ resourceReserver.getResourceAvailabilities().get(0)
							.getFirstDayAvailable());
		}
	}

}
