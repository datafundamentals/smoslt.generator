package smoslt.generator.print;

import java.util.Map;
import java.util.Set;

import smoslt.domain.Schedule;

public class PrintAssignments {

	public static void go(Schedule schedule) {
		System.out.println("HERE ARE " + schedule.getAssignments().size() +" TASK ASSIGNMENTS, AND THEIR RESPECTIVE RESOURCES");
		Map<Long, Set<Long>> assignments = schedule.getAssignments();
		for (long taskId : assignments.keySet()) {
			System.out.println(taskId);
			Set<Long> resources = assignments.get(taskId);
			for (long resourceId : resources) {
				System.out.println("\t" + resourceId);
			}
		}
	}
}
