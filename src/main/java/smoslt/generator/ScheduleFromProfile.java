package smoslt.generator;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import smoslt.domain.ProjectProfile;
import smoslt.domain.Resource;
import smoslt.domain.Schedule;
import smoslt.domain.ScheduleBuild;
import smoslt.domain.Task;
import smoslt.mprtxprt.ExportIntoProjectLibre;
import smoslt.util.ObjectCloner;

public class ScheduleFromProfile implements ScheduleBuild {

	private List<Task> taskList = new ArrayList<Task>();
	private Set<Resource> resources = new HashSet<Resource>();
	private ProjectProfile projectProfile;
	public static final String PROJECT_PROFILE_JSON_FILE_NAME = "projectProfile_a.json";
	private Schedule schedule;

	public ScheduleFromProfile(ProjectProfile projectProfile) {
		this.projectProfile = projectProfile;schedule = new Schedule(this);
	}

	@Override
	public List<Task> getTasks() {
		return taskList;
	}

	@Override
	public Set<Resource> getResources() {
		return resources;
	}

	public void execute(File file) {
		if(!file.getParentFile().exists()){
			file.getParentFile().mkdirs();
		}
		new PeelOutResourcesForPL(projectProfile, this).execute();
		new GenerateTasksFromProfile(projectProfile, schedule).execute();
		new AssignTasks(schedule).execute();
		ExportIntoProjectLibre exportIntoProjectLibre = new ExportIntoProjectLibre(
				getScheduleClone());
		exportIntoProjectLibre.go();
		exportIntoProjectLibre.export(file);
	}

	Schedule getScheduleClone() {
		Schedule scheduleClone = null;
		try {
			schedule = ((Schedule) ObjectCloner.deepCopy(schedule));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return schedule;
	}

}
