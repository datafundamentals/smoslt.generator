package smoslt.generator.helper;

/*
 * no op
 N -  will also become fodder for the stacker?
 N -  act in combination with each other to cause an abort?
 N -  act in combination with each other to create or modify tasks?
 N -  are in bad shape and need more context?
 N -  require that I stop now and add new factors?
 N -  require shape shifting at phase and roles?
 N -  jump into the area of maintenance after development?
 N -  fit as part of the constellation group?
 */
import smoslt.domain.Models;
import smoslt.domain.ProjectFactor;
import smoslt.domain.ProjectProfile;
import smoslt.domain.Schedule;
import smoslt.generator.GeneratorHelper;

public class BudgetingStyleGenerator extends GeneratorHelper {

	public BudgetingStyleGenerator() {
	}

	public BudgetingStyleGenerator(ProjectFactor projectFactor,
			ProjectProfile projectProfile, Schedule schedule,
			String phase) {
		this.projectFactor = projectFactor;
		this.projectProfile = projectProfile;
		this.schedule = schedule;
		this.resources = schedule.getResources();
		this.taskList = schedule.getTaskList();
		this.phase = phase;
		go();
	}

	void go() {
		System.out.println("THERE WERE " + resources.size());
		switch (phase) {
		case PHASE0_INITIATION:
			break;
		case PHASE1_SYSTEM_CONCEPT_DEVELOPMENT:
			break;
		case PHASE2_PLANNING:
			break;
		case PHASE3_REQUIREMENTS_ANALYSIS:
			break;
		case PHASE4_DESIGN:
			break;
		case PHASE5_DEVELOPMENT:
			for (String model : getModels()) {
				addTask(3, 0, model + "Controller", 1, Models.RESOURCE_DEV_IMPL);
			}
			break;
		case PHASE6_INTEGRATION_TEST:
			break;
		case PHASE7_IMPLEMENTATION:
			break;
		case PHASE8_OPERATIONS_MAINTENANCE:
			break;
		case PHASE9_DISPOSITION:
			break;
		}
	}

}