package smoslt.generator.helper;
import smoslt.domain.ProjectFactor;
import smoslt.domain.ProjectProfile;
import smoslt.domain.Schedule;
import smoslt.generator.GeneratorHelper;

public class AgileDefinedAsGenerator extends GeneratorHelper {

	public AgileDefinedAsGenerator() {
	}

	public AgileDefinedAsGenerator(ProjectFactor projectFactor,
			ProjectProfile projectProfile, Schedule schedule,
			String phase) {
		this.projectFactor = projectFactor;
		this.projectProfile = projectProfile;
		this.schedule = schedule;
		this.resources = schedule.getResources();
		this.taskList = schedule.getTaskList();
		this.phase = phase;
		go();
	}

	void go() {
		System.out.println("THERE WERE " + resources.size());
		switch (phase) {
		case PHASE0_INITIATION:
			break;
		case PHASE1_SYSTEM_CONCEPT_DEVELOPMENT:
			break;
		case PHASE2_PLANNING:
			break;
		case PHASE3_REQUIREMENTS_ANALYSIS:
			break;
		case PHASE4_DESIGN:
			break;
		case PHASE5_DEVELOPMENT:
//			if (eq(PF_AGILE_DEFINED_AS, 0)) {
//				addTask(30, 0, "Added time due to misunderstanding of agile", 1,
//						Models.RESOURCE_OPS);
//				addTask(30, 0, "Added time due to misunderstanding of agile", 2,
//						Models.RESOURCE_DEV_IMPL);
//				addTask(3, 0, "Added time due to misunderstanding of agile", 1,
//						Models.RESOURCE_QA_TEST);
//			} else if (eq(PF_AGILE_DEFINED_AS, 2)) {
//				addTask(20, 0, "Added time due to misunderstanding of agile", 1,
//						Models.RESOURCE_OPS);
//				addTask(20, 0, "Added time due to misunderstanding of agile", 2,
//						Models.RESOURCE_DEV_IMPL);
//				addTask(2, 0, "Added time due to misunderstanding of agile", 1,
//						Models.RESOURCE_QA_TEST);
//			}else if (eq(PF_AGILE_DEFINED_AS, 3)) {
//				addTask(10, 0, "Added time due to misunderstanding of agile", 1,
//						Models.RESOURCE_OPS);
//				addTask(10, 0, "Added time due to misunderstanding of agile", 2,
//						Models.RESOURCE_DEV_IMPL);
//				addTask(10, 0, "Added time due to misunderstanding of agile", 1,
//						Models.RESOURCE_QA_TEST);
//			}
			break;
		case PHASE6_INTEGRATION_TEST:
//			if (eq(PF_AGILE_DEFINED_AS, 0)) {
//				addTask(30, 0, "Added time due to misunderstanding of agile", 1,
//						Models.RESOURCE_OPS);
//				addTask(30, 0, "Added time due to misunderstanding of agile", 2,
//						Models.RESOURCE_DEV_IMPL);
//				addTask(3, 0, "Added time due to misunderstanding of agile", 1,
//						Models.RESOURCE_QA_TEST);
//			} else if (eq(PF_AGILE_DEFINED_AS, 2)) {
//				addTask(20, 0, "Added time due to misunderstanding of agile", 1,
//						Models.RESOURCE_OPS);
//				addTask(20, 0, "Added time due to misunderstanding of agile", 2,
//						Models.RESOURCE_DEV_IMPL);
//				addTask(2, 0, "Added time due to misunderstanding of agile", 1,
//						Models.RESOURCE_QA_TEST);
//			}else if (eq(PF_AGILE_DEFINED_AS, 3)) {
//				addTask(10, 0, "Added time due to misunderstanding of agile", 1,
//						Models.RESOURCE_OPS);
//				addTask(10, 0, "Added time due to misunderstanding of agile", 2,
//						Models.RESOURCE_DEV_IMPL);
//				addTask(10, 0, "Added time due to misunderstanding of agile", 1,
//						Models.RESOURCE_QA_TEST);
//			}
			break;
		case PHASE7_IMPLEMENTATION:
			break;
		case PHASE8_OPERATIONS_MAINTENANCE:
			break;
		case PHASE9_DISPOSITION:
			break;
		}
	}

}