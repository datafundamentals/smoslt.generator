package smoslt.generator.helper;

import smoslt.domain.ProjectFactor;
import smoslt.domain.ProjectProfile;
import smoslt.domain.Schedule;
import smoslt.generator.GeneratorHelper;

public class AccountabilityStyleGenerator extends GeneratorHelper {

	public AccountabilityStyleGenerator() {
	}

	public AccountabilityStyleGenerator(ProjectFactor projectFactor,
			ProjectProfile projectProfile, Schedule schedule,
			String phase) {
		this.projectFactor = projectFactor;
		this.projectProfile = projectProfile;
		this.schedule = schedule;
		this.resources = schedule.getResources();
		this.taskList = schedule.getTaskList();
		this.phase = phase;
		go();
	}

	void go() {
		System.out.println("THERE WERE " + resources.size());
		switch (phase) {
		case PHASE0_INITIATION:
			break;
		case PHASE1_SYSTEM_CONCEPT_DEVELOPMENT:
			break;
		case PHASE2_PLANNING:
			break;
		case PHASE3_REQUIREMENTS_ANALYSIS:
			break;
		case PHASE4_DESIGN:
			break;
		case PHASE5_DEVELOPMENT:
			break;
		case PHASE6_INTEGRATION_TEST:
			break;
		case PHASE7_IMPLEMENTATION:
			break;
		case PHASE8_OPERATIONS_MAINTENANCE:
//			if (lt(PF_ACCOUNTABILITY_STYLE, 3)) {
//				addTask(3, 0, "Added time to fix accountability issues", 1,
//						Models.RESOURCE_OPS);
//				addTask(25, 0, "Added time to fix accountability issues", 3,
//						Models.RESOURCE_DEV_IMPL);
//				addTask(25, 0, "Added time to fix accountability issues", 1,
//						Models.RESOURCE_PROJ_MGR);
//			}
			break;
		case PHASE9_DISPOSITION:
			break;
		}
	}

}