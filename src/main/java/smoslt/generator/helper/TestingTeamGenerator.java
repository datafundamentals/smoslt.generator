package smoslt.generator.helper;

import smoslt.domain.ProjectFactor;
import smoslt.domain.ProjectProfile;
import smoslt.domain.Schedule;
import smoslt.generator.GeneratorHelper;

public class TestingTeamGenerator extends GeneratorHelper {

	public TestingTeamGenerator() {
	}

	public TestingTeamGenerator(ProjectFactor projectFactor,
			ProjectProfile projectProfile, Schedule schedule,
			String phase) {
		this.projectFactor = projectFactor;
		this.projectProfile = projectProfile;
		this.schedule = schedule;
		this.resources = schedule.getResources();
		this.taskList = schedule.getTaskList();
		this.phase = phase;
		go();
	}

	void go() {
		switch (phase) {
		case PHASE0_INITIATION:
			break;
		case PHASE1_SYSTEM_CONCEPT_DEVELOPMENT:
			break;
		case PHASE2_PLANNING:
			break;
		case PHASE3_REQUIREMENTS_ANALYSIS:
			break;
		case PHASE4_DESIGN:
			break;
		case PHASE5_DEVELOPMENT:
			break;
		case PHASE6_INTEGRATION_TEST:
			break;
		case PHASE7_IMPLEMENTATION:
			break;
		case PHASE8_OPERATIONS_MAINTENANCE:
			break;
		case PHASE9_DISPOSITION:
			break;
		}
	}

}