package smoslt.generator.helper;

import smoslt.domain.ProjectFactor;
import smoslt.domain.ProjectProfile;
import smoslt.domain.Schedule;
import smoslt.generator.GeneratorHelper;

public class PersistenceIndexGenerator extends GeneratorHelper {

	public PersistenceIndexGenerator() {
	}

	public PersistenceIndexGenerator( ProjectFactor projectFactor,
			ProjectProfile projectProfile, Schedule schedule,
			String phase) {
		this.projectFactor = projectFactor;
		this.projectProfile = projectProfile;
		this.schedule = schedule;
		this.resources = schedule.getResources();
		this.taskList = schedule.getTaskList();
		this.phase = phase;
		go();
	}

	void go() { 
		switch (phase) {
		case PHASE0_INITIATION:
			break;
		case PHASE1_SYSTEM_CONCEPT_DEVELOPMENT:
			break;
		case PHASE2_PLANNING:
			break;
		case PHASE3_REQUIREMENTS_ANALYSIS:
			break;
		case PHASE4_DESIGN:
			break;
		case PHASE5_DEVELOPMENT:
//			for (String model : getModels()) {
//				addTask(3, 0, model + "Controller", 1, Models.RESOURCE_DEV_IMPL);
//			}
			break;
		case PHASE6_INTEGRATION_TEST:
			break;
		case PHASE7_IMPLEMENTATION:
			break;
		case PHASE8_OPERATIONS_MAINTENANCE:
//			if (lt(PF_ACCOUNTABILITY_STYLE, 3)) {
//				addTask(3, 0, "Added time for no accountability", 1,
//						Models.RESOURCE_OPS);
//			}
			break;
		case PHASE9_DISPOSITION:
			break;
		}
	}

}