<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1415796564199" ID="ID_1038019514" MODIFIED="1415796585358" TEXT="ContentDeliveryProject">
<node CREATED="1415797856442" ID="ID_108639553" MODIFIED="1415797867653" POSITION="right" TEXT="define outputs">
<node CREATED="1415797867905" ID="ID_1008280159" MODIFIED="1415797873533" TEXT="facebook"/>
<node CREATED="1415797874081" ID="ID_1970570509" MODIFIED="1415797888277" TEXT="linked in"/>
<node CREATED="1415797888801" ID="ID_890519938" MODIFIED="1415797890677" TEXT="twitter"/>
<node CREATED="1415797891145" ID="ID_304287403" MODIFIED="1415797897373" TEXT="blog"/>
<node CREATED="1415797897937" ID="ID_1646753482" MODIFIED="1415797902189" TEXT="corporate site"/>
<node CREATED="1415797903001" ID="ID_1951391956" MODIFIED="1415797954549" TEXT="instagram"/>
<node CREATED="1415797955177" ID="ID_1145863146" MODIFIED="1415797958549" TEXT="pinterest"/>
</node>
<node CREATED="1415797962705" ID="ID_908703315" MODIFIED="1415797967700" POSITION="right" TEXT="define inputs">
<node CREATED="1415797967969" ID="ID_1970863519" MODIFIED="1415797970733" TEXT="text"/>
<node CREATED="1415797971193" ID="ID_53661058" MODIFIED="1415797973693" TEXT="message"/>
<node CREATED="1415797974609" ID="ID_1602069802" MODIFIED="1415797982677" TEXT="google docs"/>
<node CREATED="1415798001129" ID="ID_1464120580" MODIFIED="1415798002677" TEXT="email"/>
<node CREATED="1415798003017" ID="ID_1471506299" MODIFIED="1415798028293" TEXT="corporate lan"/>
<node CREATED="1415798064522" ID="ID_1080720070" MODIFIED="1415798068653" TEXT="git repo"/>
</node>
<node CREATED="1415798032137" ID="ID_1128041297" MODIFIED="1415798114098" POSITION="right" TEXT="define triggers">
<node CREATED="1415798158202" FOLDED="true" ID="ID_829998390" MODIFIED="1415798177344" TEXT="non structured">
<node CREATED="1415798051241" ID="ID_566954548" MODIFIED="1415798076797" TEXT="email"/>
<node CREATED="1415798167297" ID="ID_633882553" MODIFIED="1415798170389" TEXT="phone"/>
<node CREATED="1415798170729" ID="ID_1051994729" MODIFIED="1415798172837" TEXT="other"/>
</node>
<node CREATED="1415798077713" ID="ID_1428453383" MODIFIED="1415798093069" TEXT="submittal app"/>
<node CREATED="1415798134546" ID="ID_1549422800" MODIFIED="1415798144933" TEXT="json message"/>
</node>
<node CREATED="1415798180458" ID="ID_1674738521" MODIFIED="1415798206205" POSITION="right" TEXT="define structure protocols"/>
<node CREATED="1415798209009" ID="ID_305459227" MODIFIED="1415798215933" POSITION="right" TEXT="define instrastructure">
<node CREATED="1415798216201" ID="ID_1734849395" MODIFIED="1415798218701" TEXT="drupal"/>
<node CREATED="1415798219377" ID="ID_138488178" MODIFIED="1415798221261" TEXT="wordpress"/>
<node CREATED="1415798221913" ID="ID_80431120" MODIFIED="1415798242349" TEXT="file based">
<node CREATED="1415798242825" ID="ID_1952048983" MODIFIED="1415798245142" TEXT="html"/>
<node CREATED="1415798245993" ID="ID_419316595" MODIFIED="1415798247254" TEXT="pdf"/>
<node CREATED="1415798254177" ID="ID_1975546892" MODIFIED="1415798256630" TEXT="json"/>
</node>
<node CREATED="1415798231121" ID="ID_989155684" MODIFIED="1415798231121" TEXT=""/>
</node>
</node>
</map>
