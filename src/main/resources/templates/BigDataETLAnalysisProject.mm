<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1415796489237" ID="ID_1121936673" MODIFIED="1415796521814" TEXT="BigDataETLAnalysisProject">
<node CREATED="1415797050842" ID="ID_835452856" MODIFIED="1415797667736" POSITION="right" TEXT="for each table">
<node CREATED="1415797096865" ID="ID_1663128609" MODIFIED="1415797167405" TEXT="BusAnls: define derived fields wanted"/>
<node CREATED="1415797367730" ID="ID_1405213218" MODIFIED="1415797379893" TEXT="BusAnls: define source fields"/>
<node CREATED="1415797170025" ID="ID_1996132836" MODIFIED="1415797360133" TEXT="BusAnls: define how fields transformed"/>
<node CREATED="1415797386281" ID="ID_1708346724" MODIFIED="1415797405294" TEXT="connect with data source"/>
<node CREATED="1415797406113" ID="ID_1770897598" MODIFIED="1415797430973" TEXT="pull into tool"/>
<node CREATED="1415797432313" ID="ID_1775059850" MODIFIED="1415797443485" TEXT="create new fields"/>
<node CREATED="1415797444257" ID="ID_1966657709" MODIFIED="1415797461181" TEXT="dump to new structure"/>
<node CREATED="1415797462265" ID="ID_1756874128" MODIFIED="1415797468045" TEXT="write structure to file"/>
<node CREATED="1415797470409" ID="ID_1908849937" MODIFIED="1415797488608" TEXT="leave breadcrumbs for later work"/>
<node CREATED="1415797489657" ID="ID_1566153549" MODIFIED="1415797516895" TEXT="commit to source control"/>
<node CREATED="1415797517969" ID="ID_1321868058" MODIFIED="1415797522981" TEXT="build tests"/>
<node CREATED="1415797524713" ID="ID_1688043282" MODIFIED="1415797600510" TEXT="move to proper location?"/>
<node CREATED="1415797601417" ID="ID_1752129867" MODIFIED="1415797623733" TEXT="create new binary schema"/>
</node>
<node CREATED="1415809160149" ID="ID_1115385169" MODIFIED="1415809166608" POSITION="right" TEXT="transforms">
<node CREATED="1415809166893" ID="ID_1449665190" MODIFIED="1415809169936" TEXT="custom"/>
</node>
<node CREATED="1415809172204" ID="ID_381214376" MODIFIED="1415809176528" POSITION="right" TEXT="analysis">
<node CREATED="1415809176813" ID="ID_1088916329" MODIFIED="1415809185776" TEXT="exploration of vendors"/>
<node CREATED="1415809186461" ID="ID_54038103" MODIFIED="1415809196504" TEXT="testing against yada"/>
</node>
<node CREATED="1415809204460" ID="ID_197629948" MODIFIED="1415809207312" POSITION="right" TEXT="setups">
<node CREATED="1415809207605" ID="ID_369789445" MODIFIED="1415809213673" TEXT="individual IDE setups"/>
<node CREATED="1415809214660" ID="ID_1838620988" MODIFIED="1415809216833" TEXT="loader"/>
<node CREATED="1415809217140" ID="ID_563888653" MODIFIED="1415809232529" TEXT="security settings"/>
<node CREATED="1415809232957" ID="ID_751983342" MODIFIED="1415809238441" TEXT="daily triggers"/>
<node CREATED="1415809239021" ID="ID_1535781681" MODIFIED="1415809252865" TEXT="communication with various data suppliers"/>
<node CREATED="1415809253172" ID="ID_1715919171" MODIFIED="1415809266633" TEXT="exploraton of features"/>
</node>
</node>
</map>
