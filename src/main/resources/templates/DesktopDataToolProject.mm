<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1415796629487" ID="ID_774371085" MODIFIED="1415796662229" TEXT="DesktopDataToolProject">
<node CREATED="1415798360722" ID="ID_1092421651" MODIFIED="1415798380140" POSITION="right" TEXT="define requirements"/>
<node CREATED="1415809952724" ID="ID_1928143761" MODIFIED="1415809958880" POSITION="right" TEXT="define">
<node CREATED="1415809959436" ID="ID_1297482861" MODIFIED="1415809964472" TEXT="requirements">
<node CREATED="1415798381697" ID="ID_207492764" MODIFIED="1415798387692" TEXT="production snafus"/>
<node CREATED="1415804225389" ID="ID_1613016358" MODIFIED="1415804228688" TEXT="line status"/>
<node CREATED="1415804229333" ID="ID_1416437429" MODIFIED="1415804262232" TEXT="sales backlogs"/>
<node CREATED="1415804262805" ID="ID_1592981915" MODIFIED="1415804267544" TEXT="special orders">
<node CREATED="1415804268060" ID="ID_1401071509" MODIFIED="1415804275264" TEXT="scheduled downtime"/>
</node>
</node>
<node CREATED="1415809972476" ID="ID_1310841935" MODIFIED="1415809981543" TEXT="for each widget">
<node CREATED="1415809981851" ID="ID_1811798144" MODIFIED="1415809987488" TEXT="technology stack">
<node CREATED="1415809990411" ID="ID_819647929" MODIFIED="1415809992240" TEXT="vendor"/>
<node CREATED="1415809992612" ID="ID_1200198249" MODIFIED="1415810000848" TEXT="high level"/>
</node>
<node CREATED="1415810005972" ID="ID_490547586" MODIFIED="1415810016952" TEXT="data provider">
<node CREATED="1415810022324" ID="ID_1604732388" MODIFIED="1415810024520" TEXT="view">
<node CREATED="1415810024956" ID="ID_1268040081" MODIFIED="1415810026104" TEXT="live"/>
<node CREATED="1415810026556" ID="ID_1001768080" MODIFIED="1415810030416" TEXT="materialized"/>
</node>
</node>
</node>
</node>
</node>
</map>
