<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1415796533379" ID="ID_1911578099" MODIFIED="1415796554822" TEXT="BusinessAPIProject">
<node CREATED="1415797692210" ID="ID_1398350089" MODIFIED="1415806329132" POSITION="right" TEXT="before starting">
<node CREATED="1415797791913" ID="ID_1301841837" MODIFIED="1415797797861" TEXT="define">
<node CREATED="1415797696497" ID="ID_383162607" MODIFIED="1415797699317" TEXT="define tools"/>
<node CREATED="1415797699801" ID="ID_1368733032" MODIFIED="1415797707005" TEXT="define structures"/>
<node CREATED="1415797707713" ID="ID_98132948" MODIFIED="1415797710845" TEXT="define roles"/>
<node CREATED="1415797711577" ID="ID_614421918" MODIFIED="1415797720357" TEXT="define indexes required"/>
<node CREATED="1415797720865" ID="ID_104523191" MODIFIED="1415797754197" TEXT="define keyvalue stores"/>
<node CREATED="1415797755049" ID="ID_1276195655" MODIFIED="1415797758333" TEXT="define use cases"/>
<node CREATED="1415797758761" ID="ID_660251338" MODIFIED="1415797774909" TEXT="define speeds required"/>
<node CREATED="1415797775465" ID="ID_1410843446" MODIFIED="1415797782749" TEXT="define cost restraints"/>
<node CREATED="1415797783289" ID="ID_551617515" MODIFIED="1415797790117" TEXT="define space required"/>
</node>
</node>
</node>
</map>
