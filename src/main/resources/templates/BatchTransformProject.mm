<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1415796408054" ID="ID_409223449" MODIFIED="1415796481062" TEXT="BatchTransformProject">
<node CREATED="1415796920650" ID="ID_1986740074" MODIFIED="1415796937885" POSITION="right" TEXT="define requirements"/>
<node CREATED="1415797037730" ID="ID_1585206291" MODIFIED="1415797049941" POSITION="right" TEXT="meet about procedures"/>
<node CREATED="1415797249177" ID="ID_1079770240" MODIFIED="1415797255786" POSITION="right" TEXT="figure out toolsets required">
<node CREATED="1415807775803" ID="ID_1916800301" MODIFIED="1415807777359" TEXT="from">
<node CREATED="1415807777651" ID="ID_773351239" MODIFIED="1415807779351" TEXT="spark"/>
<node CREATED="1415807780203" ID="ID_1678000296" MODIFIED="1415807786079" TEXT="map reduce"/>
<node CREATED="1415807786569" ID="ID_1146939286" MODIFIED="1415807791599" TEXT="pig"/>
<node CREATED="1415807792083" ID="ID_1284799114" MODIFIED="1415807802942" TEXT="hive"/>
</node>
</node>
<node CREATED="1415797256808" ID="ID_1156201141" MODIFIED="1415797269044" POSITION="right" TEXT="figure out where all the data lives"/>
<node CREATED="1415797050842" ID="ID_835452856" MODIFIED="1415807662011" POSITION="right" TEXT="for each table">
<node CREATED="1415797096865" ID="ID_1663128609" MODIFIED="1415797167405" TEXT="BusAnls: define derived fields wanted"/>
<node CREATED="1415797367730" ID="ID_1405213218" MODIFIED="1415797379893" TEXT="BusAnls: define source fields"/>
<node CREATED="1415797170025" ID="ID_1996132836" MODIFIED="1415797360133" TEXT="BusAnls: define how fields transformed"/>
<node CREATED="1415797386281" ID="ID_1708346724" MODIFIED="1415797405294" TEXT="connect with data source"/>
<node CREATED="1415797406113" ID="ID_1770897598" MODIFIED="1415797430973" TEXT="pull into tool"/>
<node CREATED="1415797432313" ID="ID_1775059850" MODIFIED="1415797443485" TEXT="create new fields"/>
<node CREATED="1415797444257" ID="ID_1966657709" MODIFIED="1415797461181" TEXT="dump to new structure"/>
<node CREATED="1415797462265" ID="ID_1756874128" MODIFIED="1415797468045" TEXT="write structure to file"/>
<node CREATED="1415797470409" ID="ID_1908849937" MODIFIED="1415797488608" TEXT="leave breadcrumbs for later work"/>
<node CREATED="1415797489657" ID="ID_1566153549" MODIFIED="1415797516895" TEXT="commit to source control"/>
<node CREATED="1415797517969" ID="ID_1321868058" MODIFIED="1415797522981" TEXT="build tests"/>
<node CREATED="1415797524713" ID="ID_1688043282" MODIFIED="1415797600510" TEXT="move to proper location?"/>
<node CREATED="1415797601417" ID="ID_1752129867" MODIFIED="1415797623733" TEXT="create new binary schema"/>
</node>
<node CREATED="1415807671786" ID="ID_138194667" MODIFIED="1415807674767" POSITION="right" TEXT="setup">
<node CREATED="1415807674971" ID="ID_1046557114" MODIFIED="1415807683295" TEXT="first transforms"/>
<node CREATED="1415807683690" ID="ID_1825807369" MODIFIED="1415807709670" TEXT="establish binary protocols"/>
<node CREATED="1415807718091" ID="ID_1441844985" MODIFIED="1415807731671" TEXT="automated transform utilities">
<node CREATED="1415807732274" ID="ID_933157969" MODIFIED="1415807737615" TEXT="flat tables"/>
<node CREATED="1415807738123" ID="ID_1802227963" MODIFIED="1415807751087" TEXT="json sources"/>
</node>
</node>
</node>
</map>
